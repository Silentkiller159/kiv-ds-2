import socket
import time
import sys
import math
# -------------------------------------------------------
# CREATOR:      Milan "Silent" Hotovec
# PROJECT NAME: Project S.I.D.O. (Silent Industries DIstributed Operations)
# -------------------------------------------------------
# List of protocol commands
# HELLO_THERE;jmeno
#  Hello paket s představením se, slaves ignorují, je to broadcast
# OH_HI_MARK;barva
#  Uvítací odpověď od mastera, dodá barvu, je to unicast
# PING
#  Ping, je to unicast
# PONG
#  Odpoved na ping, je to unicast
# COLOR;barva
#  Nove obarveni, unicast, mel by prijit od mastera
# I_HAVE_THE_HIGH_GROUND
#  Vyzva k degradaci na slave, asi se jednalo o dalsiho mastera
# -------------------------------------------------------

server = socket.socket(socket.AF_INET, socket.SOCK_DGRAM, socket.IPPROTO_UDP)
server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEPORT, 1)
# Enable broadcasting mode
server.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
server.bind(("", 37020))

encoding = "utf-8"              # Preffered encoding to be same everywhere 
msgHelloWaitTime = 5            # Wait time for hello response
msgWaitTime = 1                 # Wait time for normal responses
pingCycles = 10                 # Number of cycles to trigger pinging slaves/master
color_RED = "RED_COLOR"         # Constant for RED color name
color_GREEN = "GREEN_COLOR"     # Constant for GREEN color name
printPingPong = False           # Option to print ping pong messages

meMaster = False                # Flag if current instance is the master
masterAddress = ""              # Address of the master (Used for slave checking the master)
myColor = "init"                # Color of the current instance, starting as init
masterPingUnresponsed = 0       # Counter of master unresponded ping messages
pingUnresponsedLimit = 3        # Treshold of unresponded messages

cycleCounter = 0                # Counter of executing cycles
nodesCurrent = 1                # Counter of the active nodes in the DS
nodesRed = 0                    # Counter of the RED collored nodes in the DS
nodesGreen = 1                  # Counter of the GREEN collored nodes in the DS
nodesToColor = {}               # Dictionary, map of the address to color (key=address, value=color)
nodesPingUnresponded = {}       # Dictionary, map of the address to unresponded pings (key=address, value=countOfTheUnrespondedPings)

# Set a timeout so the socket does not block
# indefinitely when trying to receive data.
server.settimeout(msgHelloWaitTime)

print("Starting SIDO (Silent Industries Distributed Operations)...")
# The app should not end, unless we want to!!!!
while True:
    # Init the variables for each start
    meMaster = False
    cycleCounter = 0
    masterPingUnresponsed = 0
    myColor = "init"
    nodesCurrent = 1
    nodesRed = 0
    nodesGreen = 1
    nodesToColor = {}
    nodesPingUnresponded = {}
    print("MY COLOR: " + myColor)

    # Send Hello message
    messageHello = ("HELLO_THERE;" + socket.gethostname()).encode(encoding)
    server.sendto(messageHello, ('<broadcast>', 37020))
    print("Hello send, waiting for response ... " + str(msgHelloWaitTime) + " second")
    # Wait for Hello response (OH_HI_MARK), or timeout (No hello response provied), also consume other messages 
    while True:
        try:
            message, address = server.recvfrom(1024)
            msgCommand = str(message, encoding).split(";")[0]
            if msgCommand == "HELLO_THERE":
                # We will ignore Hello
                print("Consumed another HELLO_THERE broadcast ...")
                continue
            elif msgCommand == "OH_HI_MARK":
                # We want this, server response
                print("Recieved OH_HI_MARK from " + str(address) + ": " + str(message))
                masterAddress = address
                myColor = str(message, encoding).split(";")[1]      # Accept the color
                break
            else:
                print("Recieved something unexpected from " + str(address) + ": " + str(message))
        except socket.timeout:
            print("No one here, so I am the senate!")
            myColor = color_GREEN
            print("MY COLOR: " + myColor)
            meMaster = True
            break

    # Print all messages
    sys.stdout.flush()
    server.settimeout(msgWaitTime)

    # Master loop, waits for messages
    # Processes all the messages, can degrade to the slave state
    while meMaster:
        sys.stdout.flush()
        try:
            message, address = server.recvfrom(1024)
            message = str(message, encoding)
            # print("Recieved something from " + str(address) + ": " + message)
            if message.split(";")[0] == "HELLO_THERE" and message != str(messageHello, encoding):
                # Process all HELLOs which arent ours (Due to broadcast)
                print("Recieved HELLO_THERE from " + str(address))
                if nodesToColor.get(address):
                    # If slave is allready registered, just remind him his collor
                    messageToClient = ("OH_HI_MARK;" +  nodesToColor[address]).encode(encoding)
                    print("--> Allready registered, colloring him as: " + nodesToColor[address])
                else:
                    # New slave wants to be a part of DS, add, init and collor him
                    nodesCurrent = nodesCurrent + 1
                    nodesPingUnresponded[address] = 0
                    shouldBeGreen = math.ceil(nodesCurrent / 3)
                    if shouldBeGreen > nodesGreen:
                        # If new slave should be green, collor as green
                        nodesGreen = nodesGreen + 1
                        messageToClient = ("OH_HI_MARK;" + color_GREEN).encode(encoding)
                        nodesToColor[address] = color_GREEN
                        print("--> Colloring him as: " + color_GREEN)
                    else:
                        # If new slave should be red, collor as red
                        nodesRed = nodesRed + 1
                        messageToClient = ("OH_HI_MARK;" + color_RED).encode(encoding)
                        nodesToColor[address] = color_RED
                        print("--> Colloring him as: " + color_RED)
                # Send the collor welcome to the slave
                server.sendto(messageToClient, address)

                # Print the changed state to the administrator
                print("------------------------------------------------------")
                print("Current Distributed system state: ")
                print("MASTER-> me: " + myColor)
                for key in nodesToColor:
                    print("SLAVE-> " + str(key) + ": " + nodesToColor[key])
                print("------------------------------------------------------")

            elif message.split(";")[0] == "HEALTH":
                # Send the health response
                server.sendto(("OK\n").encode(encoding), address)
            elif message.split(";")[0] == "PING":
                # Send the PONG, only if the slave asked for it
                if address in nodesToColor:
                    server.sendto(("PONG").encode(encoding), address)
            elif message.split(";")[0] == "PONG":
                # Reset the PONG counter for my slave to zero
                if nodesPingUnresponded.get(address):
                    nodesPingUnresponded[address] = 0
                if printPingPong:
                    print("PONG ACCEPTED")
            elif message.split(";")[0] == "OH_HI_MARK":
                # Oh why??? I AM THE MASTER!!! Degrade you impostor ...
                print("Another master has been discovered! Sending degrade message to: " + str(address))
                server.sendto(("I_HAVE_THE_HIGH_GROUND").encode(encoding), address)
            elif message.split(";")[0] == "I_HAVE_THE_HIGH_GROUND":
                # I am not the master? Ok then .. I will be slave ...
                print("Another master is there, I will degrade to slave state ...")
                break
            elif message == str(messageHello, encoding):
                # Ok, thats my check, ignore this
                ignoreThis = True
            else:
                print("UNKNOWN MESSAGE! (" + message + ")")

            sys.stdout.flush()
        except socket.timeout:
            #print("No message :(")
            sys.stdout.flush()

        cycleCounter = cycleCounter + 1
        if cycleCounter == pingCycles:
            cycleCounter = 0
            # Checking the slaves
            deadNodes = []
            for key in nodesPingUnresponded:
                # Find the dead slaves
                if nodesPingUnresponded[key] > pingUnresponsedLimit:
                    print("Slave " + str(key) + " DEAD!")
                    deadNodes.append(key)

            for nodeKey in deadNodes:
                # Clean all dead slaves from DS (Internal counters etc. ...)
                nodesCurrent = nodesCurrent - 1
                if nodesToColor[nodeKey] == color_GREEN:
                    nodesGreen = nodesGreen - 1
                else:
                    nodesRed = nodesRed - 1
                nodesToColor.pop(nodeKey)
                nodesPingUnresponded.pop(nodeKey)
            if len(deadNodes) > 0:
                # Recollor the DS slaves if needed
                # we have the correct nodeCount and red/Green state
                print("Rebalancing the RED/GREEN states ...")
                shouldBeGreen = math.ceil(nodesCurrent / 3)
                for nodeKey in nodesToColor:
                    if shouldBeGreen == nodesGreen:
                        break
                    if nodesToColor[nodeKey] == color_RED and shouldBeGreen > nodesGreen:
                        nodesGreen = nodesGreen + 1
                        nodesRed = nodesRed - 1
                        nodesToColor[nodeKey] = color_GREEN
                        print("Changed " + str(nodeKey) + " from " + color_RED + " to " + color_GREEN)
                        server.sendto(("COLOR;" + color_GREEN).encode(encoding), nodeKey)
                    if nodesToColor[nodeKey] == color_GREEN and shouldBeGreen < nodesGreen:
                        nodesGreen = nodesGreen - 1
                        nodesRed = nodesRed + 1
                        nodesToColor[nodeKey] = color_RED
                        print("Changed " + str(nodeKey) + " from " + color_GREEN + " to " + color_RED)
                        server.sendto(("COLOR;" + color_RED).encode(encoding), nodeKey)
                print("Rebalancing DONE!")
                print("------------------------------------------------------")
                print("Current Distributed system state: ")
                print("MASTER-> me: " + myColor)
                for key in nodesToColor:
                    print("SLAVE-> " + str(key) + ": " + nodesToColor[key])
                print("------------------------------------------------------")

            # Pinging the slaves
            for key in nodesToColor:
                if nodesPingUnresponded[key] > 0:
                    print("Slave " + str(key) + " skipped ping response count: " + str(nodesPingUnresponded[key]))
                nodesPingUnresponded[key] = nodesPingUnresponded[key] + 1
                server.sendto(str.encode("PING"), key)
            # Checking if another master is there
            server.sendto(messageHello, ('<broadcast>', 37020))

    if meMaster == False:
        print("I feel like I am a slave :/")
        print("My master is: " + str(masterAddress))
        print("MY COLOR: " + myColor)
        # Slave loop
        # Accepts the PING/PONG and new COLOR instructions from master
        # Consumes HELLO_THERE ...
        while True:
            sys.stdout.flush()
            try:
                message, address = server.recvfrom(1024)
                message = str(message, encoding)
                msgCommand = message.split(";")[0]
                # print("Recieved something from " + str(address) + ": " + message)
                if msgCommand == "PING":
                    server.sendto(str.encode("PONG"), address)
                elif msgCommand == "PONG":
                    masterPingUnresponsed = 0
                    if printPingPong:
                        print("Server PONG recieved")
                elif msgCommand == "HELLO_THERE":
                    # Ssssh .... I am not a master, I am not listening ... LA LA LA ...
                    ignoreThis = True
                elif msgCommand == "COLOR":
                    if address == masterAddress:
                        myColor = message.split(";")[1]
                        print("MY NEW COLOR: " + myColor)
                elif msgCommand == "HEALTH":
                # Send the health response
                server.sendto(("OK\n").encode(encoding), address)
                else:
                    print("UNKNOWN MESSAGE! (" + message + ")")
                sys.stdout.flush()
            except socket.timeout:
                #print("No message :(")
                sys.stdout.flush()

            cycleCounter = cycleCounter + 1
            if cycleCounter == pingCycles:
                if masterPingUnresponsed > 0:
                    print("Master skipped ping response count: " + str(masterPingUnresponsed))
                if masterPingUnresponsed == pingUnresponsedLimit:
                    break
                cycleCounter = 0
                masterPingUnresponsed = masterPingUnresponsed + 1
                server.sendto(str.encode("PING"), masterAddress)
        print("Master is pressumed dead, searching another master ...")