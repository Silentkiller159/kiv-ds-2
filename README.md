# KIV-DS-2 - Master/Slave DS colloring with failure detection and recovery

# Zadání

Upravte řešení z 1. SP tak, aby splnilo podmínky níže:
- Implementujte "failure detector" tak, aby umožnil detekci výpadku libovolného uzlu (master i slave)
- Na základě výpadku se provede buď nová volba mastera, nebo přebarvení uzlů, tak aby byl zachován poměr green:red (1/3 ku 2/3)
- Umožněte monitorování služby/služeb v kontejneru pomocí Docker HEALTHCHECK, viz https://docs.docker.com/engine/reference/builder/#healthcheck
  Vyjádřete komunikační náročnost implementovaného distribuovaného algoritmu (algoritmů)  

Základní podmínky:
-  Zdrojový kód včetně dokumentace bude uložen v repozitáři https://github.com/ nebo https://gitlab.kiv.zcu.cz/
-  Dokumentace v souboru README.md v kořenovém adresáři. Musí obsahovat:
   -  Popis způsobu řešení/algoritmy jednotlivých problémů (volba mastera, barvení uzlů)
   -  Pokud není zajištěno automatické sestavení aplikace před jejím spuštěním, popis sestavení
   -  Popis spuštění a ověření funkčnosti aplikace
-  Minimální počet uzlů systému je 3 a je možné jej změnou hodnoty parametru změnit.
-  Každý uzel vypisuje do logu (standardní výstup) zřetelně každou změnu svého stavu, včetně stavu v jakém
začal (např. “init”, “master”, “green”, “red”)
-  Po ukončení volby mastera a obarvení uzlů, vypíše master do logu informaci o tom, že aplikace je hotova  
  
Technické podmínky:
-  Využití nástrojů Vagrant a Docker pro vytvoření a spuštění infrastruktury.
-  Sestavení aplikace musí být možné v prostředí Unix/Linux

# Základní info

Aplikaci jsem pojal možná maličko jinak než chtělo zadání, ale doufám že bude takto OK. Myšlenka je mít nasazeno několik instancí stejné aplikace a aby se mezi sebou dohodly, kdo bude master. Následně master přijímá podřízené, slaves, kterým přiřazuje okamžitě po připojení barvy. Aplikace je odolná vůči výpadku mastera i slaves. Počet instancí není předem nutné definovat, aplikace dokáže pracovat s dynamickým množstvím slaves. Všechny instance vypisují důležité informace na standartní výstup, master při každé změně počtu svých slaves vypíše aktuální tabulku stavu celého DS.

Aplikace integruje tzv. PING/PONG systém, takže je oboustraně hlídáno, jestli druhá strana neukončila svou činnost. (Výpadek mastera, či jednoho ze slaves)

Tabulka příkazů protokolu  
| Název | Popis |
|---|---|
| HELLO_THERE;jmeno | Broadcast pro zjištění mastera na síti, ````jmeno```` je název instance které hledá mastera |
| OH_HI_MARK;barva | Pozdrav od mastera, přijal nás jako slave. Barva je ````barva```` kterou nám přidělil (GREEN/RED) |
| PING | Test jestli druhá strana ještě žije, měla by odpověďet PONG |
| PONG | Odpověď na příkaz PING, test jestli ještě žiju |
| COLOR;barva | Příkaz na přebarvení slave, měl by přijít od mastera. Nová barva je ```barva``` |
| I_HAVE_THE_HIGH_GROUND | Příkaz od mastera, na degradaci jiného mastera do stavu slave |
| HEALTH | Kontrola zdraví, odpovídá se zprávou OK |

# Jak to zapnout

Řešení vyžaduje Docker a Vagrant. Počet nodes v systému se dá nastavit parametrem BACKENDS_COUNT ve Vagrantfile. Defaultně je zde 6 node. 

Ve složce řešení stačí zavolat příkaz ```vagrant up``` a provede se nasazení instancí ve vlastní síti v dockeru.

Vypnutí je analogické, příkaz ```vagrant destroy -f``` provede vypnutí a smazání celého řešení z prostředí docker.

## Jak to otestovat

Instance jsou nasazeny v prostředí docker, všechny výpisu jsou vidět v GUI Docker Desktop v sekci Logs (Logy kontejneru).  
Až naběhnou všechny instance, 1. zapnutá by měla být master. Master by měl vypisovat tabulku se svými slaves a měli by zde být všichni vidět, včetně jejich barev. Každý slave by měl vypsat, že si našel mastera a jakou barvu dostal.   
Je zde možnost zkusit "zabít" master instanci a pozorovat, který vyhraje jako master a ten by si měl postupně přiřadit všechny slaves které v systému ještě jsou. Instance starého mastera by po startu měla poznat, že už zde je master a měla by se stát jeho slavem.  
V případě výpadku mastera se může stát že se "omylem" zvolí více než 1 master, ti by měli tuto nekalost zjistit a umírat, dokud neskončí jen 1 master.  
Také je zde zajímavější možnost, zabít jednoho ze slaves a pozorovat jak si s tím master poradí, jestli bude třeba někoho přebarvit či nikoliv.  
Instance je možné libovolně zapínat a vypínat a sledovat jak si systém s tímto chováním poradí.  
Příkazem v konzoli, ```docker ps``` lze zjistit zdraví kontejnerů, v případě že je vše v pořádku, vedle času běhu je uvedelo healthy.

# Řešení problému

Aplikace používá pro komunikace sockety, přesněji UDP. Pro úvodní dotaz je zde broadcast zpráva na všechny v síti s dotazem kdo je master. Zbytek komunikace probíhá unicastem mezi masterem a slavem.

## Problém volby mastera

Zvolit mastera je poměrně problém, aplikace jsou všechny stejné, takže je potřeba je nějak donutit se dohodnout. Můj přístup funguje tak, že se instance zeptá jestli je na síti master. Tuto zprávu pošle broadcastem na definovaném portu (37020). Pokud master existuje, dostane odpoveď a tím se stává slavem nalezeného mastera, obsahem odpovědi je i barva kterou jí master přidělil. Pokud odpověď nedostane, dá se předpokládat, že master ještě neexistuje a instance se stává masterem, který odpovídá na broadcasty dalších instancí, které hledají mastera a přiděluje jim barvy.

V případě, že se na síti objeví více masterů, je potřeba tento problém vyřešit. Master tudíž mimo naslouchání také čas od času (10 cyklů) vyšle broadcastem dotaz na hledání mastera, neměla by dostat odpověď. Pokud se ovšem najde někdo kdo odpoví, další master, je nově nalezený master okamžitě požádám o ukončení činnosti a převedení se do stavu slave.

## Problém obarvení

Zadání vyžaduje 1/3 obarvit zeleně a 2/3 červeně, s tím že master je vždy zelený a počet zelených ze zaokrouhluje nahoru.

Master má díky tabulce slaves přehled o počtu svých slaves a proto dokáže kdykoliv zjistit jakou barvu má mít nově připojený slave. Takže při připojení slave, pošle žádost o nalezení mastera. Master odpoví uvítáním, spolu s přidělenou barvou. Rovněž si aktualizuje svou tabulku slaves, aby věděl stále o všech.

## Problém výpadku mastera

V případě výpadku mastera, dá se poznat tím, že neodpoví více jak 3x za sebou na PING, je potřeba zvolit nového mastera. Návrh aplikace stačí na to aby se toto vyřešilo samo, v případě že slave zjistí že jeho master je odpojen, hledá nového, pokud ho nenajde sám se stává masterem. V případě že ho najde, stává se slavem nalezeného mastera. Je to stejný systém jako při startu systému.

## Problém výpadku slave

Tento problém je trochu horší, master musí jednou za čas (10 cyklů), provést PING svých slaves. Pokud mu nějaký ze slaves neodpoví 3x za sebou PING, je považovám za zemřelého. Provede se odstranění z interních struktur mastera. Rovněž je potřeba provést kontrolu rozdělení barev. V případě že rozpoležení není 1/3 zelených a 2/3 červených, dojde k vyvážení. Slave od mastera může přijmout příkaz k přebarvení se. 

## Problém zjištění zdraví

Vcelku snadné, potřebujeme ověřit nejen že kontejner běží, ale že i služba v něm pracuje a není například zamrzlá. Ve zprávach byla přidána zpráva ```HEALTH```, na kterou aplikace musí odpovědět OK, pokud žije. Tento test je potřeba provést mimo aplikace, ovšem stále uvnitř kontejneru. K tomu nám pomůže NetCat (Příkaz nc). Pomocí něj se navážeme UDP spojení na portu aplikace (37020) a odešleme na něj zprávu ```HEALTH```, aplikace má 5 vteřin na to odpovědět, pokud odpoví, vše je v pořádku. Pokud ne, návratová hodnota příkazu bude 1, signalizující slehání. Je zde ještě jedna zrada, chování příkazu nc se liší mezi RedHat a Debian-based systémy, nc v RedHat nechtěl čekat na odpověď, proto je zde ten pevný čas 5 vteřin.

# Komunikační náročnost

Aplikace se snaží být vcelku minimalistická, co se počtu zpráv na síti týče. Možná až trochu moc, ale je to součást návrhu. Prvotní broadcast je celkem nezbytný a i když se šíří sítí, je to nutnost kvůli použitému algoritmu pro volbu mastera, ostatní zprávy jsou unicastem.  
Co se počtu unicast zpráv týče, posílá se nutné minimum. Přiřazení barev a případné řídící zprávy (Degradace mastera, v případě duplicity). Ve chvíli kdy je systém stabilní se ale odesílají zprávy kontrolující životnost instancí. Tyto zprávy se odesílají velmi frekventovaně a v případě velkého množství uzlů jich bude na síti mnoho. Rostou s počtem uzlů, navíc jsou oboustranné. Master kontroluje své slaves a slave si ještě kontroluje mastera. V rámci optimalizace zpráv je tohle to nejpodstatnější co by mohlo být v případě zahlcení sítě potřeba vyřešit. Pokud by master úkoloval své slaves, aby prováděly nějakou činnost, každá potvrzená činnost by se dala považovat za úspěšný ping a tím zredukovat potřebu pingů slaves. Analogicky, pokud je slave úkolován, nepotřebuje tak často kontrolovat svého mastera.

# Závěr

Apliakci jsem pojal jako distribuovaný systém, který se ze všech sil snaží neukončit činnost a fungovat i v případě výpadku mastera/slaves a snažící se vždy dostat do konzistentního stavu. Napsat apliakci, která se dá instancovat a umí se dohodnout kdo bude master a pak provést kolorizaci byla docela výzva a dost mě to bavilo. Snad je to takto OK.

